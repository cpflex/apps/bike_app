@ECHO OFF
if [%1]==[] goto usage

set specfile="cphub_tag_spec.json"
set tag=%1
ECHO Pushing Bike Application version %1
cphub push -v -s %specfile% cpflexapp "./bike_app.bin" /traxmate/apps/bike_app:%tag%

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
