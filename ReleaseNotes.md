# Traxmate Bike Application Release Notes

### V1.071a, 231209
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V1.063 - 231013
1. Updated CT102X firmware reference to v2.3.0.1 since v2.3.0.0 was a bad build.
2. Updated Flex Platform: v1.4.1.0

### V1.062 - 231007
1. Added CT102X target support version v2.3.0.0
2. Flex Platform: v1.4.0.3
3. Standard EU band ADR enabled.
   
### V1.061 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### V1.051, 230502
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4

### V1.050 230417
1. Updated Firmware Bundle to V2.0.15.0a
   - Reworked positioning control to ensure OPL is shutdown to conserve power.
   - Includes updates from alpha bundle v2.0.14.0a.
2. Updated to platform SDK version v1.3.0.4

### V1.049 230125 ###
1. References updated firmware bundle to v2.0.10.3
   - Adds production support for temperature
2. References platform SDK version v1.2.13.0
   - Fixes Downlink (health ping) polling interval initialization.
  
### V1.048 230118 ###
1. References updated firmware bundle to v2.0.9.1
   - Fixes hibernation power drain issue.
   - Fixes foundation bugs.
### V1.047 230103 ###
1.  References updated v2.0.9.0 Bundle fixes foundation bugs.

### V1.046 221227 ###
1.  References updated v2.0.8.4 firmware bundle supporting manufacturing BVT bugfixes and stabilization improvements.
2.  Uses V1 Library v1.2.11.1.

### V1.045a 221220
1.  Alpha Release Candidate with refactored archive and syslog subsystem.
2.  References updated v2.0.8.0 Bundle & STM Firmware using the V2 Stack.
3.  Uses V1 Library v1.2.11.0

### V1.044 221006
This is a release candidate for testing using the V2 Stack bundle.
1. Updated Firmware Bundle to v2.0.7.5 V2 Stack bundle..
2. Updated platform SDK version v1.2.10.0 


### V1.043 221006
1. Updated Firmware Bundle to v2.0.6.4 V2 stack.

### V1.042 221004
1. Updated Firmware Bundle to v2.0.6.3 V2 stack.

### V1.040a 220930
1. Updated firmware to v2.0.6.1a V2 stack. For testing.
2. Update platform SDK version v1.2.8.0

---
## V1 Firmware Stack (deprecated)

### V1.039 220824
Re-released with all latest changes since previous version suspect.  This new version is refactored a bit with updated variable tags.

### V1.038 220808

Updated firmware bundle for Helium deployment.

1. Updated firmware bundle to v1.0.4.0
   -  Attempts to fix burn down issue.  No significant functional changes.
   -  Bug fix for Helium runaway uplinks.  Pushing out to verify that it works.<br/>
   https://codepoint.atlassian.net/jira/software/projects/KP/boards/5?selectedIssue=KP-251
   
### V1.037 220125 ###

Updates to firmware bundle being used for Helium testing.  Implements many improvements.

1) Updated firmware bundle to v1.0.2.2a
   -  Changed EE868 TX MIN & Default data rate from DR0 to DR3
   -  Changed LoRaWAN DR defaults to be region specific (US915 DR1, EU868 DR3)
   -  Changed EU868 LoRaWAN TX & IDLE & Restricted Duty Cycle timings to 30 seconds
   -  Disabled problematic Lorawan_proc _procMac special error condition checking code
   -  Updated Flex_TelemAttachDefault schedule to harmonize with v2 stack
   -  Changed the reset time from 10 seconds to 15 seconds.
   -  Changed the default very-long press time to 3 seconds. 
2. Platform version v1.2.6.2

### V1.036 211013 ###
1. Updated Firmware reference to v1.0.0.31, with expanded check for transmitter confirmation 
   failure.  Now checks restricted state too.

### V1.035 211010 ###
1. Updated Firmware reference to v1.0.0.30, with check for transmitter failure.

### V1.034 210909 ###
1. Changed Test build to start in Normal Mode instead recovery.
2. Fixed innactivity interval bug, now 180 seconds for test build, 1200 for normal.

### V1.033 210907 ###
1. Recovery mode location reports now confirmed always. Previously 1 of 3 were confirmed.
   Normal mode is the same all location reports are confirmed.
2. Added telemetry log message to report when 0 measurements were made.
3. Added 24hr innactivity monitor to ensure location is reported at least once 
   in a 24 hour period.
4. **Test build** now reduces the innactivity interval to **3 minutes**  instead of 20 minutes.  **Production
   build** innactivity interval remains at **20 minutes**.
5. Built using v1.2.4.0 of V1 platform.

### V1.032 210824 ###
1) Bugfix battery.p PowerSave enter/exit behavior.

### V1.031 210811 ###
1. Hardened battery.p PowerSave enter/exit.
2. Removed certain accelerometer log messages.

### V1.030 210810 ###

1) Added Reset administrative downlink command and updated postman collection.
2) Still working on the battery.p issue toggling between power save and non-power save when fully charged.  The reset provides a work-around.
3) Updated API spec to V1.0.2 for Bike_App including Admin.Reset Command.

### V1.029 210727 ###

1. Built using Version 1.2.3.0 of V1 Platform. Changed battery discharge reporting algorithm to only allow decreases in battery level.  This should
   reduce the number of redundant battery reports due to normal battery fluctuation.
2. References v1.0.0.29 firmware version with fix to interchip communication stability.  
3. Changed syslog configurations to allow showing of debug messages. When
   configured through downlink command.  Previously only detail messages were shown.

### V1.028 210629 ###

1. Commented out Motion sensor debug log message, which is not needed except for development
   testing. Assumption that this is causing a crashing problem in the archive code.
2. Built using Version 1.2.3.0 of V1 Platform. Changed battery discharge reporting algorithm to only allow decreases in battery level.  This should
   reduce the number of redundant battery reports due to normal battery fluctuation.
3. References v1.0.0.28 firmware version with fix to archive flash/erase chip deselect issue
   causing significant power 1mA power drain when not in use.  
4. Changed Battery monitoring intervals to release schedule to reduce unnecessary wakeups.
   * Normal discharging samples battery every 4 hours.
   * Power Save samples every 2 hours.
   * Critical mode samples every hour.


### V1.027 210621 ###

1. Now references v1.0.0.27a firmware bundle with improved battery monitoring and stability.
2. Change boot up behavior to for restoration to default mode if last known mode was disabled.  This 
   will force it into Recovery mode for Test builds and Normal mode for Release builds.

### V1.026 210616 ###
1. Function change, all downlink response messages are now onfirmed on uplink to ensure
   delivery. 

### V1.025 210615 ###
1. Bugfix,  updated downlink tracking enabled/disabled commands to ensure recovery and normal modes
   are properly mapped to Ped tracker commands.


### V1.024 210614 ###
1. Added Application log entries to indicate in-motion and idle events.  These application
   logs can be read by Nali Programmer and serve as a semi-permanent record.

2. Created release build with system logs enabled.  

### V1.023 210603 ###
1. Now references v1.0.0.26a firmware bundle with improved syslog write performance to mitigate
   potential LoRaWAN interrupt signalling issues.

### V1.021 210526 ###
1. Altered recovery mode to only report location when connected to the network.  See readme file for specific details.
2. Updated readme file.
3. Work-around: fixed cancellation of power save mode if battery is charged above 50%.  This is a V1 library
   change, which works-around an issue where the charging event is not
   not generated properly in the firmware.
4. Added a single position acquisition after reset when in normal mode. Per
   functional changed requested by R. Windh.  The acquistion does not occur when 
   in recovery mode since it will report within a few minutes
   anyway.


### V1.020a 210519 ###
1. Altered recovery mode to only report location when connected to the network.  See readme file for specific details.
2. Updated readme file.

### V1.019a 210512 ###
1. Added uplink response when system log downlinks are received.
2. Confirmed sys logs are enabled automatically when test mode is compiled.
3. Updated battery charge complete behavior to solid LED
4. Updated to support Bike App API v1.0.1
5. Added 3rd build with system logs on and off.
6. Bugfix request tracking config downlink handler.
7. Added telemetry status messages on init & controller state changes.
8. Built with V1.0.0.23a N100 firmware bundle. See N100 firmware documentation
  for additional information.



### V1.002a 210412 ###
1. Fixed coding bug on Inverted Power Saving mode disabling the radio when set to charger.  Still needs verification.
2. Restored no motion timings on controller exit motion state to 20 minutes.

### V1.001a 210411 ###
This release provides two editions:  
* cp_v1.001a -- Release version defaults to IDLE mode when installed.
* cp_v1.001a_test -- Test version defaults to RECOVERY mode when installed. 

Notable changes:

1. References V1.0.0.10a N100 firmware bundle. See N100 firmware documentation
  for additional information.
2. Built using CP-Flex Platform version v1.2.0.0.
3. Rewrite of original bike app to leverage CP-Flex platform library
4. Updated functionality to match specifications for the App. See Readme.md.
5. Improved compile function to pull version and build information from the command line.
6. Added CP-Flex version in application header.

---
*Copyright 2019-2021, Codepoint Technologies,* 
*All Rights Reserved*
