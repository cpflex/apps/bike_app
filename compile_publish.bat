@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling /  Publishing Release Version Application and Marking as Latest.
call compile %1
call cphub_push %1
cphub update traxmate/apps/bike_app:%1 -l

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^> 
exit /B 1
