/**
*  Name:  ConfigLinkCheck.i
*
*  This module is the proprietary property of Codepoint Technologies
*  Copyright (C) 2019 Codepoint Technologies
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

/*************************
* Link Check Configuration 
**************************/

const LINKCHECK_MOTION_INTERVAL		= 144;		//Maximum tracking threshold in minutes

const LINKCHECK_MOTION_UPLINK		= 3;		//Performs a linkcheck based on uplink count while in motion. The value is the 
												//count of uplinks between a link check.  This is in addition to the 
												//time-based behavior.
/* NOT USED BY SimpleTracking.p
const LINKCHECK_STATIONARY_INTERVAL		= 720;	//Interval to perform link check when not moving. 
												//Not supported by all tracking modules.

const LINKCHECK_STATIONARY_UPLINK		= 10;	//Performs a linkcheck based on uplink count while stationary. The value is the 
												//count of uplinks between a link check.  This is in addition to the 
												//time-based behavior.
												
*/