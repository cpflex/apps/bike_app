/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Traxmate Bike Application CP-Flex Application Protocol
*  nid:        traxmate-n100-bike-app
*  uuid:       38b09d00-5c1e-4dd3-ae41-ce8140c8ea5d
*  ver:        1.0.2.1
*  date:       2021-08-11T20:32:55.449Z
*  product: Nali N100
* 
*  Copyright 2021 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_PedCmdsBatteryCharging = 1,
	NID_PedCmdsBattery = 2,
	NID_PedCmdsConfigNomintvl = 3,
	NID_PedCmdsConfigEmrintvl = 4,
	NID_PedCmdsConfig = 5,
	NID_PedCmdsEmergency = 6,
	NID_PedCmdsBatteryDischarging = 7,
	NID_PedCmdsTrackingDisabled = 8,
	NID_PedCmdsTrackingEnabled = 9,
	NID_PedCmdsTrackingActive = 10,
	NID_PedCmdsEmergencyEnable = 11,
	NID_PedCmds = 12,
	NID_PedCmdsTemperature = 13,
	NID_PedCmdsBatteryCritical = 14,
	NID_PedCmdsConfigInnactivity = 15,
	NID_PedCmdsEmergencyDisable = 16,
	NID_PedCmdsBatterylevel = 17,
	NID_PedCmdsConfigAcquire = 18,
	NID_PedCmdsTracking = 19,
	NID_ArchiveSyslogInfo = 20,
	NID_ArchiveSyslogDisable = 21,
	NID_ArchiveErase = 22,
	NID_ArchiveSyslog = 23,
	NID_ArchiveSyslogAlert = 24,
	NID_PedCmdsBatteryCharged = 25,
	NID_ArchiveSyslogAll = 26,
	NID_ArchiveSyslogDetail = 27,
	NID_AdminReset = 28
};
