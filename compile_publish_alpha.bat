@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling /  Publishing ALPHA Release Version Application and Marking as Latest. 
call compile %1
call cphub_push %1
REM cphub update traxmate/apps/bike_app:%1 -l  -- NOT MARKING AS LATEST.

echo ** Compiling /  Publishing ALPHA Release Version Application with SysLog Enabled 
call compile %1_syslog  SYSLOG
call cphub_push %1_syslog

echo ** Compiling /  Publishing ALPHA Test Version Application
call compile %1_test  TEST
call cphub_push %1_test

echo ** Compiling /  Publishing ALPHA Test Version Application with SysLog Enabled 
call compile %1_test_syslog  TEST SYSLOG
call cphub_push %1_test_syslog


goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^> 
exit /B 1
