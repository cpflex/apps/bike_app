# Traxmate Malmostad Bike Application
**Target: CT100X, CT102X Series Devices**<br/>
**Version: 1.070a<br/>**
**Release Date: 2023/12/06**
Application implements tracking functions needed for managing Malmostad bicycles deployed throughout
the region.  The purpose of the application is to keep track of the bikes and provide a recovery mode if lost
or stolen.  Implementation is designed to maximize battery life given cold weather.


## Key Features  

1. Approximately 12 month battery life between charges.
2. Two operating modes:normal and recovery.  Modes are managed by Traxmate application.
3. Configurable recovery mode tracking interval (minimum 3 minutes)
4. Eight (8) location reports per day on average -- normal mode reports when stopped more than an twenty minutes, every report is confirmed. 
5. Network check minimum 4 times a day, location report at least once per day.
6. Charging, discharging, and battery status (every 5%) reports.
7. Power saving mode at 10%, disables location reports unles explictly requested.
8. Network status indicator
9. Battery status indicator
10. Cache messages when out of network except when in recovery mode.
11. Sends initial location message immediately after reset.
 
Battery life is calculated as follows given a nominal 6,000 locates under normal operating conditions.

```
days life = (total locates)x(efficiency margin)/(average locates per day).
```
**Given:**

* total locates = ~6,000
* efficiency margin = 0.5 (cold battery conditions)
*  average locates per day = 8 
```   
 days life = ~6000 x 0.5 / 8 = ~375 days (about a year)
```
## Operating Mode Specifications
The Bike has two operating modes which governs behavior.   They are defined as follows.

**NOTE:** Test build and production builds now start in Normal mode.

### Normal Mode
Normal operating mode reports location (confirmed) when a device has stopped (motionless) for more 
20 minutes (3 minutes for test build).  Assuming a bike makes eight (8) trips per day on average, there 
will be about 8 reports each day.   The device will perform a network check a minimum  
four (4) times per day.  Every location report in normal mode is a confirmed/archived message.
Will send a log message if it failed to acquire measurements.

### Recovery
The network can command the device to enter into a recovery mode, where the tag will report every three (the default) minutes
when on network.  No locations are reported or cached when when off network. Location archiving is disabled so only the current location
is reported. Confirmation of messages follows standard confirmation pattern (every 3rd message).


## Remote Management Features
To assist with managing the devices in the field, these capabilities are provided. Traxmate system 
administrators can access these functions.
1. Enable / Disable System Logging (disabled by default)
2. Erase Archive Cache
3. Traxmate CP-Flex Protocol Compliant (v1.0)


# User Interface 
This application is meant to be used with both the Flex Tracker (CT100X) and Micro Tracker (CT102X) series of tags. Certain UI capabilities will vary depending upon the device support. 

Normal mode is entered when the device first boots after install, the device will remember its operating mode even if reset.  The operating mode is managed by the Traxmate application.

## Button Descriptions (CT1000 Only)

The following table summarizes the user interface for the application.  See the sections below for more detailed information regarding each button action.  

There are two user actions battery and network status that can be accessed
using either of the buttons.

| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Battery Level**                   | One (1) quick press  | any button or both | Battery status (see Battery Indicator below).    |  
| **Network Coverage**                | Two (2) quick press  | any button or both | Network status (see Coverage Indicator below)    |  
| **Network Reset**                   | hold > 15<br>seconds | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds | any button or both | Resets the network and configuration to factory defaults for the application.     |

### Test Mode Additional Button Actions(CT1000 Only)
When the device is compiled in TEST mode, additional button actions are enabled to
accelerate testing.   They are shown in the table below.  

| **Command**                         |     **Action**        |     **Button**     |  **Description**           |
|-------------------------------------|:---------------------:|:------------------:|---------------------------|
| **Set Normal Mode**                 | Three (3) quick press | any button or both | Sets the device into normal operating state.    |  
| **Set Recovery Mode**               | Five (5) quick press  | any button or both | Sets the device into recovery operating state.    |  

## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads on CT1000.   Only LED on CT102X series.
* **LED #2** - Lower LED, close to charger pads on CT1000.   Not supported CT102X.

| Indication                 | LEDS     | Description |
|:---------------------------| :-------:|:----------------------------|
| One second orange blink    | LED #2   | Battery charging            |
| Continuous green           | LED #2   | Battery fully charged       |
| Green blink three times    | both     | Device reset                |

See status indicators below for additional LED signalling when requesting battery and network status.

### Battery Status Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge     | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |

### Network Status Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #1 will blink as follows to indicate coverage information

| LED #1   | Signal Strength (dBm) | Description                                |
|----------|-----------------------|--------------------------------------------|
|  4 green |  -64  to -30          | Very strong signal strength                |
|  3 green |  -89  to  -65         | Good signal strength                       |
|  2 green |  -109  to -90         | Low signal strength                        |
|  1 green |  -120 to -110         | Very low signal strength                   |
|  1 red   |                       | Network unavailable (out of range)         |
|  2 red   |                       | LoRaWAN telemetry disabled.                |

## Building and Publishing
The Bike app has two build modes: normal and test.  Test mode starts the device
in recovery mode and provides additional button actions.

### Compiling the App
To compile the app use the compile executable with the following command line:

    compile <version: eg. v1.234> [TEST] [SYSLOG]

The command line requires a version tag and optional flags in the specified order:

* `TEST` - defines a test build
* `SYSLOG` - enables system logs by default.
versus the normal release.

### Compiling and Publishing.  
To build and publish both release and test apps simultaneously use the Compile_Publish.bat.  This ensures version and build configuration
are handled consistently for the release and test targets.

    compile_publish <version: eg. v1.234 >

### Manual Publish in the CP-Flex Hub 
**Deprecated -- Use compile_publish**

Use the command line tool cphub_push.bat to publish the current build in the hub.  Be sure to
update references in cphub_tag_spec.json.  The command line for the batch file is

    cphub_push <version: eg. v1.234 or v1.234_test>

Note once a build is official edit the batch file and add the -l flag to mark the 
normal release (not the test) release  as the latest.

---
*Copyright 2021, Traxmate,* 
*All Rights Reserved*
  
